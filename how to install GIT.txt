How to install GIT

1. Go to https://git-scm.com
2. Choose Downlods
3. Choose platform (Linux, Windows, Mac OS, Solaris) 
4. Download file for choosen platform
5. Run saved file. The installing process starts
6. Click next twice
7. Under "Select Components" check 
   - Associate .git* configuration files with the default text editor
   - Associate .sh files to be run with Bash 
8. Click Next
9. Choose Use Git from Git Bash only
10. Choose Checkout as-is, commit Unux-stlye line endings
11. Ready
