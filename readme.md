Definition of Done
==================

An item is done when it has been pushed to the master branch of the repository
with a comment.
